# Java Spring Boot Spring Data JPA
A Spring Boot Web API project using Spring Data JPA. Assignments are located [here](Assignments.md).

# Getting Started
- Create a schema in your local MySQL DB called `bookstore`
- Clone this repo
- Create `application.properties` file:
  - Create a file called `application.properties` based from [application.properties.template](./src/resources/application.properties.template)
  - Update the `xxx` in this file to the correct values for your local dev envionment
- Run the `PopulateDB()` unit test to populate the tables with data in your local databse
- Observe the tables in your MySQL database:
```
select * from book;
select * from badge;
select * from employee;
select * from bookstore;
select * from bookstore_book;
```
- Import the [Bookstore API](./docs/Bookstore.postman_collection.json) into Postman
![](./docs/postman.png)

# Bookstore API Architecture
![](./docs/api-arch.png)

# Bookstore Entity Relationship Diagram (ERD)
![](./docs/bookstore-erd.png)

- The [Bookstore](./src/main/java/com/example/demo/Model/Bookstore.java) has a many-to-many relationship with the [Book](./src/main/java/com/example/demo/Model/Book.java)
- The [Bookstore](./src/main/java/com/example/demo/Model/Bookstore.java) has a uni-directional one-to-many relationhip with the [Employee](./src/main/java/com/example/demo/Model/Employee.java)
- The [Employee](./src/main/java/com/example/demo/Model/Employee.java) has a uni-directional one-to-one relationship with the [Badge](./src/main/java/com/example/demo/Model/Badge.java)

Note: A bidirectional relationship is accomplished by adding a `mappedBy` attribute in the model class.

# Derived Queries
You can add these types of methods in your repository interface to query your model objects:
- [Query Methods](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods)
- [Baeldung](https://www.baeldung.com/spring-data-derived-queries)
- Quick Reference:
```
findBy<Your property>
findBy<Your property>Containing
findBy<Your property>StartingWith
findBy<Your property>EndingWith
findBy<Your property>Like
findBy<Your property>LessThan
findBy<Your property>LessThanEqual
findBy<Your property>In
findBy<Your property>Between
findBy<Your property>NotNull
findBy<Your property>Is
findBy<Your property>Equals
findBy<Your property>IsNot
findBy<Your property>IsNull
findBy<Your property>IsNotNull
findBy<Your property>{Or|And}<Your other property>
findBy<Your property>OrderByAsc<Your other Property>
findBy<Your property>OrderByDesc<Your other Property>
```

# Hibernate
- [JPA & Hibernate: Entity Lifecycle Model](https://youtu.be/Y7PpjerZkc0)
- Lifecycle states:
  - Transient
  - Managed 
  - Detached
  - Removed

# HTTP URL  & API Interop Reference
**PathVariable** 

GET http://localhost:8080/api/v1/persons/1
```
@GetMapping("api/v1/persons/{id}") 
public ResponseEntity<Person> getPerson(@PathVariable("id") Integer id) {
  // TODO...
}
```
**RequestParam**

GET http://localhost:8080/api/v1/persons?name=fred
```
@GetMapping("api/v1/persons")
public ResponseEntity<Person> getPerson(@RequestParam(required = true) String name) {
  // TODO...
}
```
**RequestBody**

POST http://localhost:8080/api/v1/persons
```
@PostMapping("api/v1/persons")
public ResponseEntity<Person> addPerson(@RequestBody Person person) {
  // TODO...
}
```

# Links
- [Spring Data JPA Tutorial | Full In-depth Course](https://youtu.be/XszpXoII9Sg)
- [Generated Values Strategy Types](https://ngdeveloper.com/generationtype-identity-vs-generationtype-sequence-vs-generationtype-auto/)
- [Hybernate Life Cycle](https://www.javatpoint.com/hibernate-lifecycle)
- [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.3/reference/htmlsingle/#web)
- [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.3/reference/htmlsingle/#data.sql.jpa-and-spring-data)
- [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
- [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
- [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
- [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)
- [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
- [Creating your first Spring Boot API](./spring-init.md)

# Notes
- Collections are lazy loaded by default.
- `findbyId()` vs `getById()`
  - Find fetches the data from the DB
  - Get is more of a lazy load approach
- Make sure you understand the hibernate life cycle
- `@Embedded/@Embedable` model objects occupy the same row as their parent object
