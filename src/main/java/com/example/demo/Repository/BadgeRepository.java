package com.example.demo.Repository;

import com.example.demo.Model.Badge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BadgeRepository extends JpaRepository<Badge, Integer> {

}
