package com.example.demo.Controller;

import com.example.demo.Model.Book;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.example.demo.Service.BookstoreService;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class BookController {

    @Autowired
    private BookstoreService bookStoreService;

    @GetMapping("api/v1/books/{id}")
    public ResponseEntity<Book> getBook(@PathVariable("id") Integer id) {
        var book = bookStoreService.getBook(id);
        return new ResponseEntity<Book>(book, HttpStatus.OK);
    }

    @PostMapping("api/v1/books")
    public ResponseEntity<Book> addBook(@RequestBody Book book) {
        bookStoreService.addBook(book);
        return new ResponseEntity<Book>(book, HttpStatus.OK);
    }
}
