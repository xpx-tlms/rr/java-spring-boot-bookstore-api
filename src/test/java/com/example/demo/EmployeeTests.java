package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.example.demo.Model.Badge;
import com.example.demo.Model.Employee;
import org.junit.jupiter.api.Test;
import com.example.demo.Repository.EmployeeRepository;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootTest
public class EmployeeTests {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void addEmployee() {
        var employee = new Employee("Joe", "Perry");
        employeeRepository.save(employee);
    }

    @Test
    void addEmployeeBadge() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        var badge = new Badge(true, format.parse("09-12-2022" ), null);
        var employee = new Employee("Paul", "Simmon");
        employee.addBadge(badge);
        employeeRepository.save(employee);
    }
}
