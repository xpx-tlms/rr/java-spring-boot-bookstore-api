# Java Spring Boot API Assignments: Bookstore

# Ex. 1: Return 404 For Book Endpoint
Return an HTTP status code `404 Not Found` (versus a `500 Internal Server Error`) for the following endpoint in the `BookController` 
when a Book Id is not present in the database for this endpoint:
```
Method: GET
URL: /api/v1/books/{id}
```

# Ex. 2: Validation Logic for Book Endpoint
Add validation logic to for the following endpoint in the `BookController` to ensure that the Book passed into this endpoint 
contains a valid ISBN and Title.
```
Method: POST
URL: /api/v1/books/
```
If the data passed into this endpoint fails the validation logic, the endpoint returns an HTTP status code `400 Bad Request`.

Create and save requests in Postman that demonstrate this logic.

#### What is the format of the ISBN?
Every ISBN consists of thirteen digits and whenever it is printed it is preceded by the letters ISBN. 
The thirteen-digit number is divided into four parts of variable length, each part separated by a hyphen.

The four parts of an ISBN are as follows:

- Group or country identifier which identifies a national or geographic grouping of publishers;
- Publisher identifier which identifies a particular publisher within a group;
- Title identifier which identifies a particular title or edition of a title;
- Check digit is the single digit at the end of the ISBN which validates the ISBN.

#### What is the format for a title?
A word or group of words that must contain at least three alphanumeric characters.

# Ex. 3: New Book Endpoint: Delete
Create the following endpoint in the `BookController` that deletes a Book for the Book Id passed into the endpoint:
```
Method: DELETE
URL: /api/v1/books/{id}
```
The endpoint returns an HTTP status code of `200` for a successful deletion.  It returns a `404` if the Book Id is not 
present in the database.  It returns a `400` if the Id is not a valid `Integer` (e.g. a `String`).

Add this endpoint to your Postman collection.

# Ex. 4: New Book Endpoint: Update
Create the following endpoint in the `BookController`:
```
Method: PUT
URL: /api/v1/books/
```
This endpoint updates the Book in the database for the data passed into this endpoint.  If the Book id passed into this
endpoint is not found in the database, this endpoint returns an HTTP status code of `404 Not Found`.

Add this endpoint to your Postman collection.

# Ex. 5: New Employee Endpoint: Select
Create the following endpoint for the Employee model:
```
Method: GET
URL: /api/v1/employees/{id}
```
The endpoint returns an HTTP status code of `200` for a successful retrieval.  It returns a `404` if the Employee Id is 
not present in the database.  It returns a `400` if the Id is not a valid `Integer` (e.g. a `String`).

Add this endpoint to your Postman collection.

# Ex. 6: New Employee Endpoint: Add
Create the following endpoint for the Employee model:
```
Method: POST
URL: /api/v1/employees
```
The endpoint returns a `400` if the Bookstore Id for the Bookstore is not present in the database, or if the 
firstname or lastname is missing for the Employee.  The endpoint returns the Employee with the newly inserted Id.

Add this endpoint to your Postman collection.

# Ex. 7: New Bookstore Endpoint: Select Bookstore
Create the following endpoint for the Bookstore model:
```
Method: GET
URL: /api/v1/bookstores/{id}
```
Return an HTTP status code `404 Not Found` (versus a `500 Internal Server Error`) when a Bookstore Id is not present in 
the database.

Add this endpoint to your Postman collection.

# Ex. 8: New Bookstore Endpoint: Add Bookstore
Create the following endpoint for the Bookstore model:
```
Method: POST
URL: /api/v1/bookstores
```
The endpoint returns a `400` if the Bookstore has any empty fields, otherwise it returns a `200`. The endpoint returns
the Bookstore with the newly inserted Id.

Add this endpoint to your Postman collection.

# Ex. 9: New Bookstore Endpoint: Delete Bookstore
Create the following endpoint for the Bookstore model:
```
Method: DELETE
URL: /api/v1/bookstores/{id}
```
The endpoint returns a `404` if the Bookstore Id is not present in the database, otherwise it returns a `200`. 

Add this endpoint to your Postman collection.

# Ex. 10: New Bookstore Endpoint: Add Book
Create the following endpoint for the Bookstore model:
```
Method: POST
URL: /api/v1/bookstores/{id}/book
```
The endpoint adds a new Book to the database and assigns it to the Bookstore Id specified in the URL.

The endpoint returns a `404` if the Bookstore Id is not present in the database, otherwise it returns a `200`.
Add this endpoint to your Postman collection.

Add this endpoint to your Postman collection.

# Ex. 11: Unit Tests
Did you finish early?  Write unit tests that test your service layer (you followed the API architecture, right?)
